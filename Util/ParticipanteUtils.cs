﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Connections;
using Asamblea_CargaCSV;
using System.IO;
using System.Text.RegularExpressions;

namespace Util
{
    public class ParticipanteUtils
    {
        private static int insert(Participante P)
        {
            try
            {
                //id_accionista int
                //nombre varchar
                //direccion varchar
                //altura int
                //piso	varchar(50)	
                //dptoUoficina	varchar(50)	
                //observaciones varchar
                //id_tipoDocumento int
                //numero_documentoint
                //telefono int
                //email varchar
                //id_tipo_participante int
                //id_localidad

                object[] Parametros = new object[12];
                Parametros[0] = P.nombre;
                Parametros[1] = P.direccion;
                Parametros[2] = P.altura;
                Parametros[3] = P.piso;
                Parametros[4] = P.dptoUoficina;
                Parametros[5] = P.observaciones;
                Parametros[6] = P.id_tipoDocumento;
                Parametros[7] = P.numero_documento;
                Parametros[8] = P.telefono;
                Parametros[9] = P.email;
                Parametros[10] = P.id_tipo_participante;
                Parametros[11] = P.id_localidad;

                DataTable result = ConnectionsHelper.executeQuery("Nombre_StoredPRocedure", Parametros).Tables[0];
                int idAccionista;
                bool? success = int.TryParse(result.Rows[0][0].ToString(), out idAccionista);
                if (success == true)
                {
                    return idAccionista;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                LogUtilities.Log("Carga_CSV - ParticipanteUtils - insert - Error: " + ex.Message);
                return 0;                
            }
        }

        private static void AccionistasXSociedadesInsert(int idSociedad, int idParticipante, int? idTipoVoto, int? cantidad)
        {
            try
            {
                object[] parametros = new object[4];
                parametros[0] = idSociedad;
                parametros[1] = idParticipante;
                parametros[2] = idTipoVoto;
                parametros[3] = cantidad;

                ConnectionsHelper.executeNonQuery("Nombre_StoredPRocedure", parametros);
            }
            catch (Exception ex)
            {
                LogUtilities.Log("Carga_CSV - ParticipanteUtils - AccionistasXSociedadesInsert - idSociedad: " + idSociedad + " - Participante: " + idParticipante + " - Error: " + ex.Message);
            }
        }

        public static bool verificaRutaArchivo(string path)
        {
            try
            {
                if(File.Exists(path))                
                    return true;
                
                else                
                    return false;
                
            }
            catch (Exception ex)
            {
                LogUtilities.Log("Carga_CSV - ParticipanteUtils - verificaRutaArchivo - Error: " + ex.Message);
                return false;
            }
        }

        public static bool procesarArchivo(int idSociedad,string path,int tipovoto,ref StringBuilder Errores)
        {
            try
            {
                string[] csv = File.ReadAllLines(path);
                string[] campos = null;
                string error = string.Empty;
                string saltoDeLinea = "\n";
                int PorcentajeGlobal = 0; //se usa para calcular la sumatoria de los porcentajes por participante

                //inicio Leer Archivo
                for (int nroFila = 0; nroFila < csv.Length;nroFila ++)
                {
                    error = string.Empty;
                    int linea = nroFila + 1;
                    campos = csv[nroFila].Split(';'); 
                    if (campos.Length < 13) //valida cantidad de campos
                    {
                        error = "Faltan completar Campos";
                        Errores.AppendLine("Fila: " + linea + ";Error: " + error + saltoDeLinea);
                    }
                    else
                    {
                        if (campos.Length > 13) //valida cantidad de campos
                        {
                            error = "Hay demasiados Campos";
                            Errores.AppendLine("Fila: " + linea + ";Error: " + error + saltoDeLinea);
                        }
                        else
                        {
                            if (validarCampos(campos, linea, tipovoto, ref Errores, ref PorcentajeGlobal))
                            {
                                int tipodocumento = Convert.ToInt32(campos[6]);
                                if (!ValidarDuplicado(idSociedad, campos[7]))
                                {
                                    Participante p = new Participante();
                                    p.nombre = campos[0];
                                    p.direccion = campos[1];
                                    p.altura = campos[2];

                                    if (campos[3] != string.Empty) //piso puede ser null
                                        p.piso = campos[3];
                                    else
                                        p.piso = null;

                                    if (campos[4] != string.Empty) //depto puede ser null
                                        p.dptoUoficina = campos[4];
                                    else
                                        p.dptoUoficina = null;

                                    if (campos[5] != string.Empty) //observacion puede ser null
                                        p.observaciones = campos[5];
                                    else
                                        p.observaciones = null;

                                    p.id_tipoDocumento = Convert.ToInt32(campos[6]);
                                    p.numero_documento = campos[7];
                                    p.telefono = campos[8];
                                    p.email = campos[9];
                                    p.id_tipo_participante = Convert.ToInt32(campos[10]);
                                    p.id_localidad = Convert.ToInt32(campos[11]);

                                    int idParticipante = insert(p); //insert de participante
                                    if (idParticipante == 0)
                                    {
                                        error = "No se pudo insertar el participante";
                                        Errores.AppendLine("Fila: " + linea + ";Error: " + error + saltoDeLinea);
                                    }
                                    else
                                    {
                                        int cantidad = 1;
                                        if (tipovoto == 2)
                                            cantidad = Convert.ToInt32(campos[12]);
                                        AccionistasXSociedadesInsert(idSociedad, idParticipante, tipovoto, cantidad); //insert ParticipanteXSociedad
                                    }  
                                }
                                else
                                {
                                    error = "Participante duplicado";
                                    Errores.AppendLine("Fila: " + linea + ";Error: " + error + saltoDeLinea);
                                }
                            }
                        }
                    }
                }
                //fin Leer Archivo

                if (Errores.Length > 0)
                    return false;
                else
                    return true;

            }
            catch (Exception ex)
            {
                LogUtilities.Log("Carga_CSV - ParticipanteUtils - procesarArchivo - Error: " + ex.Message);
                throw new Exception(ex.Message);
            }          
        }

        private static bool validarCampos(string[] campos,int linea,int tipovoto, ref StringBuilder errores,ref int PorcentajeGlobal)
        {
            //valida vacio - longitud y tipo de dato ingresado

            string saltoDeLinea = "\n";
            string error = string.Empty;

            if(campos[0].Trim() == string.Empty) //nombre
            {
                error = "nombre no puede ser vacio";
                errores.AppendLine("Fila: " + linea + ";Error: " + error + saltoDeLinea);
            }
            else
            {
                if (campos[0].Trim().Length > 50)
                {
                    error = "nombre supera los 50 caracteres";
                    errores.AppendLine("Fila: " + linea + ";Error: " + error + saltoDeLinea);
                }
            }

            if (campos[1].Trim() == string.Empty) //direccion
            {
                error = "Dirección no puede ser vacio";
                errores.AppendLine("Fila: " + linea + ";Error: " + error + saltoDeLinea);
            }
            else
            {
                if (campos[1].Trim().Length > 50)
                {
                    error = "dirección supera los 50 caracteres";
                    errores.AppendLine("Fila: " + linea + ";Error: " + error + saltoDeLinea);
                }
            }

            if (campos[2].Trim() == string.Empty) //altura
            {
                error = "altura no puede ser vacio";
                errores.AppendLine("Fila: " + linea + ";Error: " + error + saltoDeLinea);
            }
            else
            {
                bool match = false;
                match = Regex.IsMatch(campos[2].Trim(), @"^[0-9]+$");
                if(!match)
                {
                    error = "altura debe ser numérico";
                    errores.AppendLine("Fila: " + linea + ";Error: " + error + saltoDeLinea);
                }
                else
                {
                    if (campos[2].Trim().Length > 6)
                    {
                        error = "altura supera los 6 caracteres";
                        errores.AppendLine("Fila: " + linea + ";Error: " + error + saltoDeLinea);
                    }
                }
            }

            if (campos[3].Trim() != string.Empty) //piso puede ser null
            {
                bool match = false;
                match = Regex.IsMatch(campos[3].Trim(), @"^[a-zA-Z0-9\/ñ\/Ñ]+$");
                if (!match)
                {
                    error = "piso no acepta caracteres especiales o simbolos";
                    errores.AppendLine("Fila: " + linea + ";Error: " + error + saltoDeLinea);
                }
                else
                {
                    if (campos[3].Trim().Length > 2)
                    {
                        error = "piso supera los 2 caracteres";
                        errores.AppendLine("Fila: " + linea + ";Error: " + error + saltoDeLinea);
                    }
                }
            }

            if (campos[4].Trim() != string.Empty) //depto puede ser null
            {
                bool match = false;
                match = Regex.IsMatch(campos[4].Trim(), @"^[a-zA-Z0-9\/ñ\/Ñ]+$");
                if (!match)
                {
                    error = "depto no acepta caracteres especiales o simbolos";
                    errores.AppendLine("Fila: " + linea + ";Error: " + error + saltoDeLinea);
                }
                else
                {
                    if (campos[4].Trim().Length > 2)
                    {
                        error = "depto supera los 2 caracteres";
                        errores.AppendLine("Fila: " + linea + ";Error: " + error + saltoDeLinea);
                    }
                }
            }

            if (campos[5].Trim() != string.Empty) //observacion puede ser null
            {
                if (campos[5].Trim().Length > 50)
                {
                    error = "observacion supera los 50 caracteres";
                    errores.AppendLine("Fila: " + linea + ";Error: " + error + saltoDeLinea);
                }
                
            }

            if (campos[6].Trim() != string.Empty) //tipo Documento
            {
                bool match = Regex.IsMatch(campos[6].Trim(), @"^[0-9]$");
                if (match)
                {
                    if (campos[7].Trim() != string.Empty) // nro documento
                    {
                        if (campos[6].Trim() == "4")
                        {
                            match = Regex.IsMatch(campos[7].Trim(), @"^[a-zA-Z]{3}[0-9]{6}$");
                            if (!match)
                            {
                                error = "pasaporte debe tener el formato \"AAA111111\"";
                                errores.AppendLine("Fila: " + linea + ";Error: " + error + saltoDeLinea);
                            }
                        }
                        else
                        {
                            match = Regex.IsMatch(campos[7].Trim(), @"^[0-9]+$");
                            if (!match)
                            {
                                error = "tipo Documento debe ser numérico";
                                errores.AppendLine("Fila: " + linea + ";Error: " + error + saltoDeLinea);
                            }
                        }

                        switch (campos[6].Trim())
                        {
                            case "1": //dni
                                if (campos[7].Trim().Length > 8 || campos[7].Trim().Length < 7)
                                {
                                    error = "dni debe tener entre 7 y 8 caracteres";
                                    errores.AppendLine("Fila: " + linea + ";Error: " + error + saltoDeLinea);
                                }
                                break;
                            case "2": //cuil
                                if (campos[7].Trim().Length != 11)
                                {
                                    error = "cuil debe tener 11 caracteres";
                                    errores.AppendLine("Fila: " + linea + ";Error: " + error + saltoDeLinea);
                                }
                                break;
                            case "3": //cuit
                                if (campos[7].Trim().Length != 11)
                                {
                                    error = "cuit debe tener 11 caracteres";
                                    errores.AppendLine("Fila: " + linea + ";Error: " + error + saltoDeLinea);
                                }
                                break;
                            case "4": //pasaporte
                                if (campos[7].Trim().Length != 9)
                                {
                                    error = "paraposte debe tener 9 caracteres";
                                    errores.AppendLine("Fila: " + linea + ";Error: " + error + saltoDeLinea);
                                }
                                break;
                        }
                    }
                    else
                    {
                        error = "nro de documento no puede ser vacio";
                        errores.AppendLine("Fila: " + linea + ";Error: " + error + saltoDeLinea);
                    }
                }
                else
                {
                    error = "tipo documento debe ser numérico";
                    errores.AppendLine("Fila: " + linea + ";Error: " + error + saltoDeLinea);
                }
            }
            else
            {
                error = "tipo documento no puede ser vacio";
                errores.AppendLine("Fila: " + linea + ";Error: " + error + saltoDeLinea);
            }

            if (campos[8].Trim() != string.Empty) //telefono
            {
                bool match = Regex.IsMatch(campos[8].Trim(), @"^[0-9]+$");
                if(match)
                {
                    if (campos[8].Trim().Length > 50)
                    {
                        error = "telefono no puede superar los 50 caracteres";
                        errores.AppendLine("Fila: " + linea + ";Error: " + error + saltoDeLinea);
                    }
                }
                else
                {
                    error = "telefono debe ser numérico";
                    errores.AppendLine("Fila: " + linea + ";Error: " + error + saltoDeLinea);
                }
            }
            else
            {
                error = "telefono no puede ser vacio";
                errores.AppendLine("Fila: " + linea + ";Error: " + error + saltoDeLinea);
            }

            if (campos[9].Trim() != string.Empty) //email
            {
                bool match = Regex.IsMatch(campos[9].Trim(), @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                if(!match)
                {
                    error = "emial no tiene el formato correcto";
                    errores.AppendLine("Fila: " + linea + ";Error: " + error + saltoDeLinea);
                }
            }
            else
            {
                error = "emial no puede ser vacio";
                errores.AppendLine("Fila: " + linea + ";Error: " + error + saltoDeLinea);
            }

            if (campos[10].Trim() != string.Empty) //tipo participante
            {
                bool match = Regex.IsMatch(campos[10].Trim(), @"^[0-9]$");
                if(!match)
                {
                    error = "Tipo participante debe ser numérico";
                    errores.AppendLine("Fila: " + linea + ";Error: " + error + saltoDeLinea);
                }
            }
            else
            {
                error = "Tipo participante no puede ser vacio";
                errores.AppendLine("Fila: " + linea + ";Error: " + error + saltoDeLinea);
            }

            if (campos[11].Trim() != string.Empty) //localidad
            {
                bool match = Regex.IsMatch(campos[11].Trim(), @"^[0-9]$");
                if (!match)
                {
                    error = "idlocalidad debe ser numérico";
                    errores.AppendLine("Fila: " + linea + ";Error: " + error + saltoDeLinea);
                }
            }
            else
            {
                error = "Tipo participante no puede ser vacio";
                errores.AppendLine("Fila: " + linea + ";Error: " + error + saltoDeLinea);
            }

            if(tipovoto == 2)
            {
                if (campos[12].Trim() != string.Empty) //porcentaje (solo para tipo voto = 2)
                {
                    bool match = Regex.IsMatch(campos[12].Trim(), @"^[0-9]+$");
                    if (!match)
                    {
                        error = "porcentaje de voto debe ser numérico";
                        errores.AppendLine("Fila: " + linea + ";Error: " + error + saltoDeLinea);
                    }
                    else
                    {
                        int porcentaje = Convert.ToInt32(campos[12].Trim());
                        if (porcentaje < 1 || porcentaje > 100)
                        {
                            error = "porcentaje de voto incorrecto";
                            errores.AppendLine("Fila: " + linea + ";Error: " + error + saltoDeLinea);
                        }
                        else
                        {
                            PorcentajeGlobal += porcentaje;
                            if(PorcentajeGlobal > 100)
                            {
                                error = "La suma de los porcentajes de voto por participante no pueden superar el 100%";
                                errores.AppendLine("Fila: " + linea + ";Error: " + error + saltoDeLinea);
                            }
                        }
                    }
                }
                else
                {
                    error = "porcentaje de voto no puede ser vacio";
                    errores.AppendLine("Fila: " + linea + ";Error: " + error + saltoDeLinea);
                }
            }

            if (error != string.Empty) //utiliza variable error como bandera
                return false;
            else
                return true;
            
        }


        private static bool ValidarDuplicado(int idSociedad,string nrodocumento)
        {
            try
            {
                DataTable dt = new DataTable();
                object[] p = new object[1];
                p[0]=idSociedad;

                dt = ConnectionsHelper.executeQuery("Nombre_StoredPRocedure", p).Tables[0];

                if(dt.Rows.Count > 0)
                {
                    foreach(DataRow dr in dt.Rows)
                    {
                        if(!DBNull.Value.Equals(dr["Documento Accionista"]))
                        {
                            string documentoCargado = dr["Documento Accionista"].ToString();
                            if(documentoCargado == nrodocumento)
                            {
                                return true;
                            }
                        }
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                LogUtilities.Log("Carga_CSV - ParticipanteUtils - ValidarDuplicado - Error: " + ex.Message);
                return true;
            }
        }

        public static bool verificaExtencionArchivo(string path)
        {
            string[] Apath = path.Split('\\');
            int namePosition = Apath.Length -1;
            string filename = Apath[namePosition];
            if (filename.ToLower().EndsWith(".csv"))
                return true;
            else
                return false;
        }
    }
}
