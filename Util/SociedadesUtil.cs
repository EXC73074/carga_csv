﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Connections;

namespace Util
{
    public class SociedadesUtil
    {
        public static bool VerificaSociedadExiste(int idSociedad, ref int tipovoto)
        {
            try
            {
                object[] p = new object[1];
                p[0] = idSociedad;
                DataTable dt = new DataTable();
                dt = ConnectionsHelper.executeQuery("Nombre_StoredPRocedure", p).Tables[0];
                if(dt.Rows.Count > 0)
                {
                    tipovoto = Convert.ToInt32(dt.Rows[0]["tipoVoto"].ToString());
                    return true;
                }                                  
                else                
                    return false;
                
            }
            catch (Exception ex)
            {
                LogUtilities.Log("Carga_CSV - SociedadesUtil - VerificaSociedadExiste - Error: " + ex.Message);
                return false;
            }
        }
    }
}
