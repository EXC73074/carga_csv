﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asamblea_CargaCSV
{
    public class Participante
    {
        //id_accionista int
        //nombre varchar
        //direccion varchar
        //altura int
        //observaciones varchar
        //id_tipoDocumento int
        //numero_documentoint
        //telefono int
        //email varchar
        //id_tipo_participante int
        //id_localidad

        public int id_accionista { get; set; }
        public string nombre { get; set; }
        public int id_localidad { get; set; }
        public string direccion { get; set; }
        public string altura { get; set; }
        public string piso { get; set; }
        public string dptoUoficina { get; set; }
        public string observaciones { get; set; }
        public int id_tipoDocumento { get; set; }
        public string numero_documento { get; set; }
        public string telefono { get; set; }
        public string email { get; set; }
        public int id_tipo_participante { get; set; }
    }
}
