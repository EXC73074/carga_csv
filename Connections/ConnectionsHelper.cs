﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Util;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

namespace Connections
{
    public class ConnectionsHelper
    {
        private static CryptoUtilities cu = new CryptoUtilities("ContraseñaCualquiera");
        private static string CadenaConexion = cu.DecryptData(ConfigurationManager.AppSettings["CadenaConexion"]);

        public static DataSet executeQuery(String consulta)
        {
            DataSet dset = SqlHelper.ExecuteDataset(CadenaConexion, CommandType.Text, consulta);
            return dset;
        }

        public static DataSet executeQuery(String nombreProcedimiento, object[] paramValues)
        {
            DataSet dset = SqlHelper.ExecuteDataset(CadenaConexion, nombreProcedimiento, paramValues);
            return dset;
        }

        public static Int32 executeNonQuery(String consulta)
        {
            int respuesta = 0;
            respuesta = SqlHelper.ExecuteNonQuery(CadenaConexion, CommandType.Text, consulta);
            return respuesta;
        }

        public static int executeNonQuery(String nombreprocedimiento, object[] paramValues)
        {
            int respuesta = 0;
            respuesta = SqlHelper.ExecuteNonQuery(CadenaConexion, nombreprocedimiento, paramValues);
            return respuesta;
        }
    }
}
