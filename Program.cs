﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Util;

namespace Asamblea_CargaCSV
{
    class Program
    {
        static void Main(string[] args)
        {
            string saltoDeLinea = "\n";
            string dobleSaltoDeLinea = "\n" + "\n";

            int IdSociedad = 0;
            int TipoVoto = 0;
            bool existe = false;

            Console.Write("CARGA MASIVA DE PARTICIPANTES" +dobleSaltoDeLinea);
            Console.Write("Ingrese el id de la Sociedad" + saltoDeLinea);
            while (!existe)
            {
                IdSociedad = Convert.ToInt32(Console.ReadLine());
                existe = SociedadesUtil.VerificaSociedadExiste(IdSociedad, ref TipoVoto);
                if (!existe)
                {                    
                    Console.Write("La sociedad no existe, ingrese otro ID."+saltoDeLinea);
                }                               
            }

            Console.Write("ingrese la ruta del CSV"+dobleSaltoDeLinea);
            string path = string.Empty;
            bool ExisteDirectorio = false;
            while (!ExisteDirectorio)
            {
                path = Console.ReadLine();
                ExisteDirectorio = ParticipanteUtils.verificaRutaArchivo(path);
                if (!ExisteDirectorio)
                {
                    Console.Write("No se encuentra el archivo con la ruta especificada." + saltoDeLinea);
                }

                ExisteDirectorio = ParticipanteUtils.verificaExtencionArchivo(path);
                if (!ExisteDirectorio)
                {
                    Console.Write("El archivo debe ser .CSV" + saltoDeLinea);
                }
            }
            
            StringBuilder Errores = new StringBuilder();
            try
            {
                bool result = ParticipanteUtils.procesarArchivo(IdSociedad, path, TipoVoto, ref Errores);
                if (!result)
                {
                    Console.WriteLine("Se encontraron los siguientes errores en el archivo" + dobleSaltoDeLinea + Errores.ToString());
                }

                Console.WriteLine(dobleSaltoDeLinea + "El proceso finalizó");
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
            
            Console.ReadKey();
        }
    }
}
